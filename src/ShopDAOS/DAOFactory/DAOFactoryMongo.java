package ShopDAOS.DAOFactory;

import ShopDAOS.DAO.comanda.ComandaDAO;
import ShopDAOS.DAO.comanda.ComandaImpMongo;
import ShopDAOS.DAO.empleat.EmpleatDAO;
import ShopDAOS.DAO.empleat.EmpleatImpMongo;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

public class DAOFactoryMongo extends DAOFactory {

    static String user = "kappacat";
    static String pass = "kappacat";
    static String host = "clusterea1.zphho.mongodb.net";

    private static MongoClient mongoClient;

    public static MongoDatabase getDatabase(){
        mongoClient = MongoClients.create("mongodb+srv://"+user+":"+pass+"@"+host+"/retryWrites=true&w=majority");
        return mongoClient.getDatabase("Shop");
    }
    public static void closeConnection(){
        mongoClient.close();
    }


    @Override
    public EmpleatDAO getEmpleatDAO() {
        return new EmpleatImpMongo();
    }

    @Override
    public ComandaDAO getComandaDAO() {
        return new ComandaImpMongo();
    }
}
