package ShopDAOS.DAOFactory;

import ShopDAOS.DAO.comanda.ComandaDAO;
import ShopDAOS.DAO.empleat.EmpleatDAO;
import ShopDAOS.DAO.empleat.EmpleatImpSQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DAOFactorySQL extends DAOFactory {

    private final String URL = "jdbc:postgresql://rogue.db.elephantsql.com:5432/"; // Ubicació de la BD.
    private final String BD = "rosnftmh"; // Nom de la BD.
    private final String USER = "rosnftmh";
    private final String PASSWORD = "4Dg87Vx4HtInBnOau_RHMRssEvXEfXJI";

    //instancia de la bdd
    private static DAOFactorySQL database = null;
    public static DAOFactorySQL getInstance(){
        if(database == null)
            database = new DAOFactorySQL();
        return database;
    }

    private Connection connection;

    //constructor
    public DAOFactorySQL(){
        connection = null;
    }

    //metode per establir la conexio amb la bdd
    public Connection connect(){
        try {
            connection = DriverManager.getConnection(URL + BD, USER, PASSWORD);
            System.out.println("\u001B[32mLa base de dades s'ha iniciat correctament.\u001B[0m\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    //metode per retornar una conexio establerta
    public Connection getConnection() {
        return connection;
    }

    //metode per terminar la conexio amb la bdd
    public void close(){
        try {
            connection.close();
            System.out.println("\u001B[32mLa base de dades s'ha tancat correctament.\u001B[0m");
        }catch(SQLException e){
            System.err.println("\u001B[31mError tancant la BD\u001B[0m");
        }
        connection = null;
    }

    @Override
    public EmpleatDAO getEmpleatDAO() {
        return new EmpleatImpSQL();
    }

    @Override
    public ComandaDAO getComandaDAO() {
        return null;
    }
}
