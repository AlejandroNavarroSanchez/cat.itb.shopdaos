package ShopDAOS.DAOFactory;

import ShopDAOS.DAO.comanda.ComandaDAO;
import ShopDAOS.DAO.empleat.EmpleatDAO;

public abstract class DAOFactory {

    // Bases de datos soportadas
    public static final int POSTGRESQL = 1;
    public static final int MONGODB = 2;

    public abstract EmpleatDAO getEmpleatDAO();
    public abstract ComandaDAO getComandaDAO();

    public static DAOFactory getDAOFactory(int bd) {
        return switch (bd) {
            case POSTGRESQL -> new DAOFactorySQL();
            case MONGODB -> new DAOFactoryMongo();
            default -> null;
        };
    }

}
