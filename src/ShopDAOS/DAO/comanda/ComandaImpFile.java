package ShopDAOS.DAO.comanda;

import ShopDAOS.DAOFactory.DAOFactorySQL;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ComandaImpFile implements ComandaDAO {
    File fileName;
    public ComandaImpFile() {
        fileName = new File("comandes.json");
    }

    @Override
    public boolean insertar(Comanda comanda) {
        return false;
    }

    @Override
    public int insertarLlista(List<Comanda> comandes) throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(comandes);

        // We create a new file with the JSON object
        Path dir = Path.of("./src/shopFitxersJSON");
        if (!Files.exists(dir)) {
            Files.createDirectory(dir);
            File file = new File("./src/shopFitxersJSON/"+fileName);
            try (PrintWriter out = new PrintWriter(new FileWriter(file))) {
                out.write(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("File creation success: " + fileName + "\n");
        } else {
            File file = new File("./src/shopFitxersJSON/"+fileName);
            try (PrintWriter out = new PrintWriter(new FileWriter(file))) {
                out.write(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("File creation success: " + fileName + "\n");
        }

        // Session close
        DAOFactorySQL.getInstance().close();
        return comandes.size();
    }

    @Override
    public boolean eliminar(int comandaId) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificar(Comanda comanda) {
        return false;
    }

    @Override
    public Comanda consultar(int comandaId) {
        return null;
    }

    @Override
    public List<Comanda> consultarLlistaPerProducte(int productID) {
        return null;
    }

    @Override
    public List<Comanda> consultarLlista() {
        List<Comanda> comandaList = new ArrayList<>();
        try {
            // Create a reader
            Reader reader = Files.newBufferedReader(Paths.get("./src/shopFitxersJSON/" + fileName));

            // Convert JSON array to list of users
            comandaList = new Gson().fromJson(reader, new TypeToken<List<Comanda>>() {}.getType());

            // Close reader
            reader.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return comandaList;
    }

}
