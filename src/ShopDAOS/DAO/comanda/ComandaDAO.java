package ShopDAOS.DAO.comanda;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface ComandaDAO {
    boolean insertar(Comanda comanda);
    int insertarLlista(List<Comanda> comandes) throws IOException; //Retorna el número d'objectes inserits

    boolean eliminar(int comandaId);
    boolean eliminarConjunt();

    boolean modificar(Comanda comanda);

    Comanda consultar(int comandaId);
    List<Comanda> consultarLlistaPerProducte(int productID);
    List<Comanda> consultarLlista() throws SQLException;

}
