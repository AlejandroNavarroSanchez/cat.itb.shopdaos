package ShopDAOS.DAO.comanda;

import ShopDAOS.DAO.empleat.Empleat;
import ShopDAOS.DAOFactory.DAOFactoryMongo;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class ComandaImpMongo implements ComandaDAO {
    MongoDatabase mongoDB;
    String coleccio = "comandes";

    public ComandaImpMongo() {
        mongoDB = DAOFactoryMongo.getDatabase();
    }

    @Override
    public boolean insertar(Comanda comanda) {
        return false;
    }

    @Override
    public int insertarLlista(List<Comanda> comandes) throws IOException {
        mongoDB.getCollection(coleccio).drop();

        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        mongoDB = mongoDB.withCodecRegistry(pojoCodecRegistry);

        // Collection
        MongoCollection<Comanda> col = mongoDB.getCollection(coleccio, Comanda.class);

        for (Comanda c : comandes) {
            System.out.println("Adding... " + c + "\n");
            col.insertOne(c);
        }
        System.out.println("Collection inserted successfully!\n");
        return comandes.size();
    }

    @Override
    public boolean eliminar(int comandaId) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificar(Comanda comanda) {
        return false;
    }

    @Override
    public Comanda consultar(int comandaId) {
        return null;
    }

    @Override
    public List<Comanda> consultarLlistaPerProducte(int productID) {
        return null;
    }

    @Override
    public List<Comanda> consultarLlista() throws SQLException {
        return null;
    }
}
