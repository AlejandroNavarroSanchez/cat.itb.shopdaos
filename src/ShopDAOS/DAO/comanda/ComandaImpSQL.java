package ShopDAOS.DAO.comanda;

import ShopDAOS.DAOFactory.DAOFactorySQL;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ComandaImpSQL implements ComandaDAO {
    Connection connection;

    public ComandaImpSQL() { connection = DAOFactorySQL.getInstance().connect();
    }

    @Override
    public boolean insertar(Comanda comanda) {
        return false;
    }

    @Override
    public int insertarLlista(List<Comanda> comandes) {
        return 0;
    }

    @Override
    public boolean eliminar(int comandaId) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificar(Comanda comanda) {
        return false;
    }

    @Override
    public Comanda consultar(int comandaId) {
        return null;
    }

    @Override
    public List<Comanda> consultarLlistaPerProducte(int productID) {
        return null;
    }

    @Override
    public List<Comanda> consultarLlista() throws SQLException {
        String query = "SELECT * FROM comanda;";
        ComandaImpSQL comSQL = new ComandaImpSQL();
        Statement statement = comSQL.connection.createStatement();
        ResultSet rs = statement.executeQuery(query);

        List<Comanda> comList = new ArrayList<>();
        while(rs.next()) {
            int id_comanda = rs.getInt(1);
            int id_producte = rs.getInt(2);
            Date data_comanda = rs.getDate(3);
            int quantitat = rs.getInt(4);
            int id_prov = rs.getInt(5);
            Date data_tramesa = rs.getDate(6);
            Double total = rs.getDouble(7);

            Comanda com = new Comanda();

            com.setId_comanda(id_comanda);
            com.setId_producte(id_producte);
            com.setData_comanda(data_comanda);
            com.setQuantitat(quantitat);
            com.setId_prov(id_prov);
            com.setData_tramesa(data_tramesa);
            com.setTotal(total);

            comList.add(com);
        }
        return comList;
    }
}
