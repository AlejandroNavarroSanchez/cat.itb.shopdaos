package ShopDAOS.DAO.proveidor;

import ShopDAOS.DAO.empleat.Empleat;
import ShopDAOS.DAO.producte.Producte;
import ShopDAOS.DAOFactory.DAOFactoryMongo;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class ProveidorImpMongo implements ProveidorDAO {
    MongoDatabase mongoDB;
    String coleccio = "proveidors";

    public ProveidorImpMongo() {
        mongoDB = DAOFactoryMongo.getDatabase();
    }

    @Override
    public boolean insertar(Proveidor prov) {
        return false;
    }

    @Override
    public int insertarLlista(List<Proveidor> provs) throws IOException {
        mongoDB.getCollection(coleccio).drop();

        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        mongoDB = mongoDB.withCodecRegistry(pojoCodecRegistry);

        // Collection
        MongoCollection<Proveidor> col = mongoDB.getCollection(coleccio, Proveidor.class);

        for (Proveidor p : provs) {
            System.out.println("Adding... " + p + "\n");
            col.insertOne(p);
        }
        System.out.println("Collection inserted successfully!\n");
        return provs.size();
    }

    @Override
    public boolean eliminar(int idProv) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificarQuantitat(Proveidor prov) {
        return false;
    }

    @Override
    public Proveidor consultar(int idProv) {
        return null;
    }

    @Override
    public List<Proveidor> consultarLlista() throws SQLException {
        return null;
    }

    @Override
    public Proveidor consultarPerIdProducte(int productID) {
        return null;
    }
}
