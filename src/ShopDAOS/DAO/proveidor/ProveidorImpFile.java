package ShopDAOS.DAO.proveidor;

import ShopDAOS.DAO.comanda.Comanda;
import ShopDAOS.DAO.comanda.ComandaImpSQL;
import ShopDAOS.DAO.empleat.Empleat;
import ShopDAOS.DAO.producte.Producte;
import ShopDAOS.DAOFactory.DAOFactorySQL;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProveidorImpFile implements ProveidorDAO {
    File fileName;
    public ProveidorImpFile() {
        fileName = new File("proveidors.json");
    }

    @Override
    public boolean insertar(Proveidor prov) {
        return false;
    }

    @Override
    public int insertarLlista(List<Proveidor> provs) throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(provs);

        // We create a new file with the JSON object
        Path dir = Path.of("./src/shopFitxersJSON");
        if (!Files.exists(dir)) {
            Files.createDirectory(dir);
            File file = new File("./src/shopFitxersJSON/"+fileName);
            try (PrintWriter out = new PrintWriter(new FileWriter(file))) {
                out.write(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("File creation success: " + fileName + "\n");
        } else {
            File file = new File("./src/shopFitxersJSON/"+fileName);
            try (PrintWriter out = new PrintWriter(new FileWriter(file))) {
                out.write(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("File creation success: " + fileName + "\n");
        }

        // Session close
        DAOFactorySQL.getInstance().close();

        return provs.size();
    }

    @Override
    public boolean eliminar(int idProv) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificarQuantitat(Proveidor prov) {
        return false;
    }

    @Override
    public Proveidor consultar(int idProv) {
        return null;
    }

    @Override
    public List<Proveidor> consultarLlista() {
        List<Proveidor> llista = new ArrayList<>();
        try {
            // Create a reader
            Reader reader = Files.newBufferedReader(Paths.get("./src/shopFitxersJSON/" + fileName));

            // Convert JSON array to list of users
            llista = new Gson().fromJson(reader, new TypeToken<List<Proveidor>>() {}.getType());

            // Close reader
            reader.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return llista;
    }

    @Override
    public Proveidor consultarPerIdProducte(int productID) {
        return null;
    }
}
