package ShopDAOS.DAO.proveidor;


import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface ProveidorDAO {
    boolean insertar(Proveidor prov);
    int insertarLlista(List<Proveidor> provs) throws IOException; //Retorna el número d'objectes inserits

    boolean eliminar(int idProv);
    boolean eliminarConjunt();

    boolean modificarQuantitat(Proveidor prov); //a partir del id de producte


    Proveidor consultar(int idProv);
    List<Proveidor> consultarLlista() throws SQLException;
    Proveidor consultarPerIdProducte(int productID);

}
