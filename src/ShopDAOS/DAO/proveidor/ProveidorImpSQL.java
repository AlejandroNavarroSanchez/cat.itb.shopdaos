package ShopDAOS.DAO.proveidor;

import ShopDAOS.DAOFactory.DAOFactorySQL;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProveidorImpSQL implements ProveidorDAO {
    Connection connection;

    public ProveidorImpSQL() { connection = DAOFactorySQL.getInstance().connect();
    }

    @Override
    public boolean insertar(Proveidor prov) {
        return false;
    }

    @Override
    public int insertarLlista(List<Proveidor> provs) {
        return 0;
    }

    @Override
    public boolean eliminar(int idProv) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificarQuantitat(Proveidor prov) {
        return false;
    }

    @Override
    public Proveidor consultar(int idProv) {
        return null;
    }

    @Override
    public List<Proveidor> consultarLlista() throws SQLException {
        String query = "SELECT * FROM prov;";
        ProveidorImpSQL provSQL = new ProveidorImpSQL();
        Statement statement = provSQL.connection.createStatement();
        ResultSet rs = statement.executeQuery(query);

        List<Proveidor> provList = new ArrayList<>();
        while(rs.next()) {
            int id_prov = rs.getInt(1);
            String nom = rs.getString(2);
            String adreca = rs.getString(3);
            String ciutat = rs.getString(4);
            String estat = rs.getString(5);
            String codi_postal = rs.getString(6);
            int area = rs.getInt(7);
            String telefon = rs.getString(8);
            int id_producte = rs.getInt(9);
            int quantitat = rs.getInt(10);
            Double limit_credit = rs.getDouble(11);
            String observacions = rs.getString(12);

            Proveidor prov = new Proveidor();

            prov.setId_prov(id_prov);
            prov.setNom(nom);
            prov.setAdreca(adreca);
            prov.setCiutat(ciutat);
            prov.setEstat(estat);
            prov.setCodi_postal(codi_postal);
            prov.setArea(area);
            prov.setTelefon(telefon);
            prov.setId_producte(id_producte);
            prov.setQuantitat(quantitat);
            prov.setLimit_credit(limit_credit);
            prov.setObservacions(observacions);

            provList.add(prov);
        }

        return provList;
    }

    @Override
    public Proveidor consultarPerIdProducte(int productID) {
        return null;
    }

}
