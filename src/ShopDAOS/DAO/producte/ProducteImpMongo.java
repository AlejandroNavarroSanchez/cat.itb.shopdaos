package ShopDAOS.DAO.producte;

import ShopDAOS.DAO.empleat.Empleat;
import ShopDAOS.DAOFactory.DAOFactoryMongo;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class ProducteImpMongo implements ProducteDAO {
    MongoDatabase mongoDB;
    String coleccio = "productes";

    public ProducteImpMongo() {
        mongoDB = DAOFactoryMongo.getDatabase();
    }

    @Override
    public boolean insertar(Producte product) {
        return false;
    }

    @Override
    public int insertarLlista(List<Producte> productes) throws IOException {
        mongoDB.getCollection(coleccio).drop();

        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        mongoDB = mongoDB.withCodecRegistry(pojoCodecRegistry);

        // Collection
        MongoCollection<Producte> col = mongoDB.getCollection(coleccio, Producte.class);

        for (Producte p : productes) {
            System.out.println("Adding... " + p + "\n");
            col.insertOne(p);
        }
        System.out.println("Collection inserted successfully!\n");
        return productes.size();
    }

    @Override
    public boolean eliminar(int productId) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificarStock(Producte product) {
        return false;
    }

    @Override
    public Producte consultar(int productID) {
        return null;
    }

    @Override
    public List<Producte> consultarLlista() throws SQLException {
        return null;
    }
}
