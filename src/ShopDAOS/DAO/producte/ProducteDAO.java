package ShopDAOS.DAO.producte;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface ProducteDAO {
    boolean insertar(Producte product);
    int insertarLlista(List<Producte> productes) throws IOException; //Retorna el número d'objectes inserits

    boolean eliminar(int productId);
    boolean eliminarConjunt();

    boolean modificarStock(Producte product);

    Producte consultar(int productID);
    List<Producte> consultarLlista() throws SQLException;
}
