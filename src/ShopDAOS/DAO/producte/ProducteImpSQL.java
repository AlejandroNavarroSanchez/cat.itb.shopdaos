package ShopDAOS.DAO.producte;

import ShopDAOS.DAOFactory.DAOFactorySQL;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProducteImpSQL implements ProducteDAO {
    Connection connection;

    public ProducteImpSQL() { connection = DAOFactorySQL.getInstance().connect();
    }

    @Override
    public boolean insertar(Producte product) {
        return false;
    }

    @Override
    public int insertarLlista(List<Producte> productes) {
        return 0;
    }

    @Override
    public boolean eliminar(int productId) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificarStock(Producte product) {
        return false;
    }

    @Override
    public Producte consultar(int productID) {
        return null;
    }

    @Override
    public List<Producte> consultarLlista() throws SQLException {
        String query = "SELECT * FROM producte;";
        ProducteImpSQL prodSQL = new ProducteImpSQL();
        Statement statement = prodSQL.connection.createStatement();
        ResultSet rs = statement.executeQuery(query);

        List<Producte> prodList = new ArrayList<>();
        while(rs.next()) {
            int id_producte = rs.getInt(1);
            String descripcio = rs.getString(2);
            int stockactual = rs.getInt(3);
            int stockminim = rs.getInt(4);
            Double preu = rs.getDouble(5);

            Producte prod = new Producte();

            prod.setId_producte(id_producte);
            prod.setDescripcio(descripcio);
            prod.setStockactual(stockactual);
            prod.setStockminim(stockminim);
            prod.setPreu(preu);

            prodList.add(prod);
        }
        return prodList;
    }

}
