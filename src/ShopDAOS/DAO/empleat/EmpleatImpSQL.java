package ShopDAOS.DAO.empleat;

import ShopDAOS.DAOFactory.DAOFactorySQL;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class EmpleatImpSQL implements EmpleatDAO {
    Connection connection;

    public EmpleatImpSQL() { connection = DAOFactorySQL.getInstance().connect();
    }

    @Override
    public boolean insertar(Empleat emps) {
        return false;
    }

    @Override
    public int insertarLlista(List<Empleat> emps) {
        return 0;
    }

    @Override
    public boolean eliminar(int empId) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificar(Empleat emp) {
        return false;
    }

    @Override
    public Empleat consultar(int empId) {
        return null;
    }

    @Override
    public List<Empleat> consultarLlista() throws SQLException {
        String query = "SELECT * FROM empleat;";
        EmpleatImpSQL empSQL = new EmpleatImpSQL();
        Statement statement = empSQL.connection.createStatement();
        ResultSet rs = statement.executeQuery(query);

        List<Empleat> empList = new ArrayList<>();
        while(rs.next()) {
            int id_empleat = rs.getInt(1);
            String cognom = rs.getString(2);
            String ofici = rs.getString(3);
            int cap_id = rs.getInt(4);
            Date data_alta = rs.getDate(5);
            int salari = rs.getInt(6);
            Double comissio = rs.getDouble(7);
            int dept_no = rs.getInt(8);

            Empleat emp = new Empleat();

            emp.setIdEmpleat(id_empleat);
            emp.setCognom(cognom);
            emp.setOfici(ofici);
            emp.setCapId(cap_id);
            emp.setDataAlta(data_alta);
            emp.setSalari(salari);
            emp.setComissio(comissio);
            emp.setDepNo(dept_no);

            empList.add(emp);
        }
        return empList;
    }

    @Override
    public void empleatsToJsonFile() throws SQLException, IOException {

    }
}
