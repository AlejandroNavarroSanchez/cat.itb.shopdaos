package ShopDAOS.DAO.empleat;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;


public class Empleat implements Serializable {

    private int _id;
    private String cognom;
    private String ofici;
    private int cap_id;
    private Date data_alta;
    private int salari;
    private Double comissio;
    private int dep_no;

    public Empleat(){}

    public Empleat(int id, String cognom, String ofici, int capId, Date dataAlta, int salari, Double comissio, int depNo) {
        this._id = id;
        this.cognom = cognom;
        this.ofici = ofici;
        this.cap_id = capId;
        this.data_alta = dataAlta;
        this.salari = salari;
        this.comissio = comissio;
        this.dep_no = depNo;
    }

    @Override
    public String toString() {
        return "Empleat{" +
                "empleatID=" + _id +
                ", cognom='" + cognom + '\'' +
                ", ofici='" + ofici + '\'' +
                ", capId=" + cap_id +
                ", dataAlta='" + data_alta + '\'' +
                ", salari=" + salari +
                ", comissio=" + comissio +
                ", depNo=" + dep_no +
                '}';
    }

    public int getIdEmpleat() {
        return _id;
    }

    public void setIdEmpleat(int id_empleat) {
        this._id = id_empleat;
    }

    public String getCognom() {
        return cognom;
    }

    public void setCognom(String cognom) {
        this.cognom = cognom;
    }

    public String getOfici() {
        return ofici;
    }

    public void setOfici(String ofici) {
        this.ofici = ofici;
    }

    public int getCapId() {
        return cap_id;
    }

    public void setCapId(int capId) {
        this.cap_id = capId;
    }

    public Date getDataAlta() {
        return data_alta;
    }

    public void setDataAlta(Date dataAlta) {
        this.data_alta = dataAlta;
    }

    public int getSalari() {
        return salari;
    }

    public void setSalari(int salari) {
        this.salari = salari;
    }

    public Double getComissio() {
        return comissio;
    }

    public void setComissio(Double comissio) {
        this.comissio = comissio;
    }

    public int getDepNo() {
        return dep_no;
    }

    public void setDepNo(int depNo) {
        this.dep_no = depNo;
    }

}





