package ShopDAOS.DAO.empleat;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface EmpleatDAO {

    boolean insertar(Empleat emps);
    int insertarLlista(List<Empleat> emps) throws IOException; //Retorna el número d'objectes inserits

    boolean eliminar(int empId);
    boolean eliminarConjunt();

    boolean modificar(Empleat emp);

    Empleat consultar(int empId);
    List<Empleat> consultarLlista() throws SQLException;

    void empleatsToJsonFile() throws SQLException, IOException;

}
