package ShopDAOS.DAO.empleat;

import ShopDAOS.DAO.comanda.Comanda;
import ShopDAOS.DAOFactory.DAOFactoryMongo;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import javax.print.Doc;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.mongodb.client.model.Filters.*;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class EmpleatImpMongo implements EmpleatDAO {
    MongoDatabase mongoDB;
    String coleccio = "empleats";

    public EmpleatImpMongo() {
        mongoDB = DAOFactoryMongo.getDatabase();
    }


    @Override
    public boolean insertar(Empleat emps) {
        return false;
    }

    @Override
    public int insertarLlista(List<Empleat> emps) {

        mongoDB.getCollection(coleccio).drop();

        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        mongoDB = mongoDB.withCodecRegistry(pojoCodecRegistry);

        // Collection
        MongoCollection<Empleat> col = mongoDB.getCollection(coleccio, Empleat.class);

        for (Empleat e : emps) {
            System.out.println("Adding... " + e + "\n");
            col.insertOne(e);
        }
        System.out.println("Collection inserted successfully!\n");

        return emps.size();
    }

    @Override
    public boolean eliminar(int empId) {
        return false;
    }

    @Override
    public boolean eliminarConjunt() {
        return false;
    }

    @Override
    public boolean modificar(Empleat emp) {
        return false;
    }

    @Override
    public Empleat consultar(int empId) {

        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        mongoDB = mongoDB.withCodecRegistry(pojoCodecRegistry);

        MongoCollection<Document> col = mongoDB.getCollection(coleccio);

        Document d = col.find(
                eq("idEmpleat", empId)
        ).first();

        Empleat e = new Empleat();
        assert d != null;
        e.setIdEmpleat((Integer) d.get("idEmpleat"));
        e.setCognom((String) d.get("cognom"));
        e.setOfici((String) d.get("ofici"));
        e.setCapId((Integer) d.get("capId"));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date utilDate = (java.util.Date) d.get("dataAlta");
        String formattedDate = sdf.format(utilDate);
        java.sql.Date date = java.sql.Date.valueOf(formattedDate);
        e.setDataAlta(date);

        e.setSalari((Integer) d.get("salari"));
        e.setComissio((Double) d.get("comissio"));
        e.setDepNo((Integer) d.get("depNo"));

        return e;
    }

    @Override
    public List<Empleat> consultarLlista() {
        return null;
    }

    @Override
    public void empleatsToJsonFile() throws SQLException, IOException {

    }
}
